#!/bin/bash

git pull
git submodule update --init

echo "Please make sure that you have rvm installed with ruby-2.4.1 installed"
rvm ls
while true; do
    read -p "is ruby 2.4.1 (rvm) installed ? (Yes/No)
" yn
    case $yn in
        [Yy]es ) break;;
        [Nn]o ) echo "please run \`$ rvm install ruby-2.4.1\` && "exit 0;;
        * ) echo "Please answer yes or no.";;
    esac
done

command -v ember >/dev/null 2>&1 || { echo >&2 "ember is required but it's not installed.
please run \`npm install -g ember-cli\`"; exit 1; }
command -v rvm use 2.4.1 >/dev/null 2>&1 || { echo >&2 "ruby 2.4.1 is required but it's not installed.\n ; exit 1; }

echo "cd ." >> ./stop.sh
chmod 755 stop.sh
./stop.sh
touch ./stop.sh
chmod 755 stop.sh
echo "#!/bin/bash" > stop.sh
mkdir -p logs
websocketd --port=8182 ruby inventory.rb >> logs/socket_server.log    & echo "kill " $! >> stop.sh
cd shoes_api
rvm 2.4.1 exec bundle install                           >> ../logs/bundle.log
rvm 2.4.1 exec ./bin/socket                             >> ../logs/socket_client.log & echo "kill " $! >> ../stop.sh
rvm 2.4.1 exec ./bin/rake db:create                     >> ../logs/rails_db.log
rvm 2.4.1 exec ./bin/rake db:migrate                    >> ../logs/rails_db.log
rvm 2.4.1 exec rails s -p 4241                          >> ../logs/rails_server.log  & echo "kill " $! >> ../stop.sh
cd ../shoes_dashboard
npm install                              >> ../logs/npm.log
ember s -p 4242                          >> ../logs/ember_server.log  & echo "kill " $! >> ../stop.sh
cd ..
echo "rm ./stop.sh" >> ./stop.sh
echo "
____________
Running app | on http://localhost:4242 (may take some time to build ember)
¯¯¯¯¯¯¯¯¯¯¯¯
"
echo "----------
IMPORTANT : use ./stop.sh to kill all 4 background runing processes
----------"
