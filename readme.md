# Shoes overviewer 

## main idea
statement of the exercise : https://github.com/mathieugagne/shoe-store

the idea :

1 - run a websocket which generate some events (sales or purchased events for specific shoes from a specific store)

2 - a rails process (webscoket client) retrieve and process them

3 - a rails api exposes some ressources

4 - an ember dashboard display some informations

## how to run

You need rails, npm, psql, websocketd and ember installed.

I build them with following versions :
- rvm : 1.29.1
- rails : 5.1.6
- ruby : 2.4.1
- npm : 5.6.0
- node : 8.11.3
- ember-cli : 3.1.4
- psql (PostgreSQL) : 9.6.2

You just need to clone this repo and run `./start.sh` with at least **rvm and ember installed**

it will run all ws client/server api dashboard etc ... in background processes

just run `./stop.sh` to kill them all

## to do

TODO list (by priority):
- [ ] implement api tests [api]
- [ ] add favicon + title [dashboard]
- [ ] move as many things as possible from ember + sementic-ui to ember-sementic-ui
- [ ] comment code
- [ ] update .gitignore [api, dashboard]
- [ ] implement inbound events tests [api]
- [ ] implement a shoes prices/list page [dashboard]
- [ ] implement a date filter in history page [dashboard]
- [ ] export data from history page (excel with dynamic crossed table) [api, dashboard, huge]
- [ ] implement env var system (instead of static urls etc ...) [api, dashboard]
- [ ] implement login/logout/sign-up
- [ ] implement dashboard tests [dashboard]
- [ ] implement component with details transactions (ups and downs)
- [ ] clean gemfile and package.json to remove usless third-part
- [ ] make a better start.sh (--verbose, -y some comments)
- [ ] make an api doc [api]
- [ ] implement I18n [dashboard]
- [ ] implement customizable dasbhaord
- [ ] make a db_exports/backup/log system (to allow clear db each x weeks/months) [api]

## to know

- I am using sementic-ui to have a nice rendered html without much work
- I am trying to use [this](https://github.com/slashsBin/styleguide-git-commit-message) to build my commits messages
- feel free to open a ticket if you have some issues, if you want new features, if you need precision on some code part, or even if you think some part need some improvments.
- feel free to make some PRs with related tests (once they will be implemented)
